json.extract! event, :id, :name, :date, :host, :location, :state, :created_at, :updated_at
json.url event_url(event, format: :json)
