Rails.application.routes.draw do

  root "users#index"
  resources :events
  resources :users
  resources :sessions

  # get 'sessions/new'
  # post "sessions" => "session#create"

  # get "users/:id" => "users#show"

end
